import java.util.*;

public class TicTacToe {

    Random r = new Random();
    boolean isAiMode = true;

    char[][] board = new char[3][3];
    int currentRow, currentCol;
    enum States {PLAY, X_WON, O_WON, DRAW};
    States currentState = States.PLAY;
    char currentPlayer = 'X';

    public TicTacToe() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++)
                board[i][j] = ' ';
        }
    }

    public static void main(String[] args) {
        TicTacToe ttt = new TicTacToe();
        ttt.start();
    }

    public void start() {
        while (currentState == States.PLAY) {
            playerMove();
            updateState();
            drawBoard();            
        }

        switch(currentState) {
            case O_WON: 
                System.out.println("Player O wins");
                break;
            case X_WON:
                System.out.println("Player X wins");
                break;
            case DRAW:
                System.out.println("Game ends in draw");
                break;
        }
    }

    public void updateState() {
        board[currentRow][currentCol] = currentPlayer;
        if (currentPlayer == 'X')
            currentPlayer = 'O';
        else
            currentPlayer = 'X';
        if (winner('X')) {
            currentState = States.X_WON;
        }
        if (winner('O')) {
            currentState = States.O_WON;
        }
        if (draw()) {
            currentState = States.DRAW;
        }
    }

    public void playerMove() {
        if (currentPlayer == 'X') {
            humanMove();
        } else {
            if (isAiMode)
                aiMove();
            else
                humanMove();
        }
    }

    public boolean draw() {
        boolean isEmpty = false;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == ' ')
                    isEmpty = true;
            }
        }
        return !isEmpty;

    }

    public boolean winner(char player) {

        for (int i = 0; i < 3; i++) {
            if (board[i][0] == player &&
                    board[i][1] == player &&
                    board[i][2] == player)
                return true;
            if (board[0][i] == player &&
                    board[1][i] == player &&
                    board[2][i] == player)
                return true;
        }

        if (board[0][0] == player &&
                board[1][1] == player &&
                board[2][2] == player)
            return true;

        if (board[0][2] == player &&
                board[1][1] == player &&
                board[2][0] == player)
            return true;

        return false;

    }

    public void humanMove() {

        Scanner sc = new Scanner(System.in);
        String str;

        int row;
        int col;

        while (true) {
            System.out.println("Enter q to quit or ");
            System.out.print("Enter row (0-2): ");
            str = sc.nextLine();
            if (str.equals("q"))
                System.exit(0);
            row = Integer.parseInt(str);
            System.out.print("Enter col (0-2): ");
            str = sc.nextLine();
            if (str.equals("q"))
                System.exit(0);
            col = Integer.parseInt(str);
            if (row < 3 && col < 3 && row >= 0 && col >= 0) {
                if (board[row][col] != ' ') {
                    System.out.println("Invalid choice!");
                } else
                    break;

            } else {
                System.out.println("Invalid choice!");

            }

        }

        currentRow = row;
        currentCol = col;
    }

    public void aiMove() {
        int row, col;
        while (true) {
            col = r.nextInt(3);
            row = r.nextInt(3);
            if (board[row][col] == ' ')
                break;

        }
        currentRow = row;
        currentCol = col;
    }

    public void drawBoard() {
        System.out.println(" 0  1  2");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j]);
                if (j != 2)
                    System.out.print("|");
            }
            System.out.println("");
            if (i != 2)
                System.out.println(" -------");
        }
        System.out.println();
    }

}