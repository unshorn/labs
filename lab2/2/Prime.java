import java.util.ArrayList;

public class Prime {
    public static void main(String[] args) {
        // Simple program to identify the first n prime numbers
        int n = 20;
        // start the loop from 2
        ArrayList<Integer> factors = new ArrayList<>();
        for (int number = 2; number <= 20; number++) {
            // simple flag to toggle between prime and composite
            boolean isPrime = true;
            // Find the factors of number
            for (int factor = 2; factor < number; factor++) {
                // Test if number is divisible by factor
                if (number % factor == 0) {
                    factors.add(factor);
                    isPrime = false;
                }
            } // end inner factor loop
            if (isPrime) {
                System.out.println(number + " is prime.");
            } else {
                System.out.println(number + " is composite.");
                System.out.println("The factors include: " + factors);
                factors.clear();
            }
        } // end outer number loop
    } // end main method
} // end class