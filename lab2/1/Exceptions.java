public class Exceptions {
    public static void main(String args[]) {

        int N = Integer.parseInt(args[0]);
        int[] array = new int[3];

        try {
            for (int i = 0; i < N; ++i) {
                array[i] = i;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("error!");
            e.printStackTrace();
        }

        for (int i = 0; i < array.length; ++i) {
            System.out.println(array[i]);
        }
    }
}