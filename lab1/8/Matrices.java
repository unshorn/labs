public class Matrices {
    public static void main(String[] args) {
        int N = 2;
        int[][] a = new int[][] { { 1, 2 }, { 3, 4 } };
        int[][] b = new int[][] { { 5, 6 }, { 7, 8 } };

        int[][] c = sum(a, b, N);

    }

    public static int[][] sum(int[][] a, int[][] b, int N) {
        int[][] c = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                c[i][j] = a[i][j] + b[i][j];
            }
        }
        return c;

    }
}