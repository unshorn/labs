import java.util.*;

public class Game {

    boolean isComputerTurn = false;
    boolean isGameOver = false;
    int humanPlayerScore = 0;
    int computerPlayerScore = 0;

    String[] choices = new String[] { "rock", "paper", "scissor" };
    Random r = new Random();

    public static void main(String[] args) {
        Game game = new Game();
        game.start();
    }

    public void start() {
        while (!isGameOver) {
            String choice = getChoice();
            String aiChoice = choices[r.nextInt(3)];
            System.out.println("Computer chooses: " + aiChoice);
            if (choice.equals("rock")) {
                if (aiChoice.equals("rock")) {
                    System.out.println("draw");
                }
                if (aiChoice.equals("paper")) {
                    System.out.println("Computer wins");
                    computerPlayerScore++;
                }
                if (aiChoice.equals("scissor")) {
                    System.out.println("Human wins");
                    humanPlayerScore++;
                }
            }
            if (choice.equals("scissor")) {
                if (aiChoice.equals("rock")) {
                    System.out.println("Computer wins");
                    computerPlayerScore++;
                }
                if (aiChoice.equals("paper")) {
                    System.out.println("Human wins");
                    humanPlayerScore++;
                }
                if (aiChoice.equals("scissor")) {
                    System.out.println("draw");
                }
            }
            if (choice.equals("paper")) {
                if (aiChoice.equals("rock")) {
                    System.out.println("Human wins");
                    humanPlayerScore++;
                }
                if (aiChoice.equals("paper")) {
                    System.out.println("draw");

                }
                if (aiChoice.equals("scissor")) {
                    System.out.println("Computer wins");
                    computerPlayerScore++;
                }
            }
            System.err.println(bonus(choice, aiChoice));
            if (computerPlayerScore >= 3 || humanPlayerScore >= 3) {
                isGameOver = true;
            }

        }
    }

    public String getChoice() {
        Scanner sc = new Scanner(System.in);
        String str;
        while (true) {
            System.out.print("Enter choice (rock, paper, scissor): ");
            str = sc.nextLine();
            if (str.equals("rock") || str.equals("paper") || str.equals("scissor"))
                break;
            else {
                System.out.println("Choice not correct!");
            }
        }

        return str;
    }

    public String bonus(String choice, String aiChoice) {
        String[][] map = new String[][] {
                { "draw", "paper", "rock" },
                { "paper", "draw", "scissor" },
                { "rock", "scissor", "draw" }
        };
        return map[stringToIndex(choice)][stringToIndex(aiChoice)];
    }

    int stringToIndex(String str) {
        int index = 0;
        switch (str) {
            case "rock":
                index = 0;
                break;
            case "paper":
                index = 1;
                break;
            case "scissor":
                index = 2;
                break;
        }
        return index;
    }

}