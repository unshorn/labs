// class scope
public class HelloWorld // class declaration
 {
    public static void main(String[] args) { // main method and parameters
        System.out.println("Hello world: " + args[0]); // System library println that prints to console
    }
}