public class TestCubes {

    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        Cubes cubes = new Cubes();
        for (int i = 1; i <= N; i++)
            System.out.println(i + " " + cubes.cube(i));
    }
}